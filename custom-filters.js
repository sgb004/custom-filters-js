/**
 * @name: CustomFilters
 * @author: sgb004
 * @version: 0.3.1
 */

export class CustomFilters {
	#__filters = {};

	constructor(target) {
		this.target = target ?? this;
	}

	dispatchFilter(args) {
		args = Array.from(args);
		let filter = args[0];
		args = args.slice(1);

		if (this.#__filters[filter]) {
			for (let fn of this.#__filters[filter]) {
				args[0] = fn.apply(this.target, args);
			}
		}
		return args[0];
	}

	addFilterListener(filter, fn) {
		if (!this.#__filters[filter]) {
			this.#__filters[filter] = [];
		}
		this.#__filters[filter].push(fn);
	}
}

# CustomFilters

This class take inspiration of CustomEvent class.

## Installation

```npm
npm i custom-filters-js
```

## Methods

### constructor( target )

If you want to can add a target, when you call to addFilterListener the target will returned as this.

### dispatchFilter

Trigger a previously registered filter.

You can send all the parameters you need, the first parameter will always be the name of the filter.

```javascript
.dispatchFilter('filterName', p1, p2, p3, ...);
```

### addFilterListener

Register a filter.

```javascript
.addFilterListener('filterName', function(p1, p2, p3){
	console.log(p1, p2, p3);
});
```

When the dispatchFilter function is called, addFilterListener will be called using its corresponding filter.
